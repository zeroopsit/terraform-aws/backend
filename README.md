# Backend
This terraform module is used to create an s3 backend with dynamodb locking

## Inputs

| Paramter | Type | Default | Required | Description |
| -------- | ---- | ------- | -------- | ----------- |
| backend_name_prefix | string | `n/a` | No | backend_name_prefix |