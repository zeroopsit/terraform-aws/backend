variable "backend_name_prefix" {
  description = "The prefix to add to the name of the s3 bucket and dynamodb table"
  type        = string
}
