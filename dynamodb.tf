resource "aws_dynamodb_table" "terraform" {
  name = "${var.backend_name_prefix}-terraform-lock"

  read_capacity  = 2
  write_capacity = 2

  hash_key  = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}
