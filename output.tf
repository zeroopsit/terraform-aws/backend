output "bucket_name" {
  value = aws_s3_bucket.terraform.id
}

output "table_name" {
  value = aws_dynamodb_table.terraform.id
}
